// Importing Module
const readline = require("readline");
const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
});

// Code here!
function greet(name, address, birthday) {
  const currentDate = new Date();
  const currentYear = currentDate.getFullYear();
  let a = name
  let b = address
  let c = currentYear - birthday;
    
  console.log("Hello, " + a + " looks like you're " + c + " And you lived in " + b + " !");
  }
          
  // DON'T CHANGE
  console.log("Goverment Registry\n");
  // GET User's Name
  rl.question("What is your name? ", (name) => {
    // GET User's Address
    rl.question("Which city do you live? ", (address) => {
      // GET User's Birthday
      rl.question("When was your birthday year? ", (birthday) => {
        greet(name, address, birthday);
  
        rl.close();
      });
    });
  });
  
  rl.on("close", () => {
    process.exit();
  });
  