/* Sphere Volume */
function calculateSphereVolume(r) {
    let phi = 3.14;
    let sphereVolume = 3/4 * phi * r ** 3;

    return sphereVolume;
}

let sphere = calculateSphereVolume(6);
let resultSphere = sphere;

console.log("Sphere Volume: " + resultSphere + " cm3.");

/* Cone Volume */
function calculateConeVolume(r, h) {
    let phi = 3.14;
    let coneVolume = 1/3 * phi * r ** 2 * h;

    return coneVolume;
}

let cone = calculateConeVolume(6, 8);
let resultCone = cone;

console.log("Cone Volume: " + resultCone + " cm3.");