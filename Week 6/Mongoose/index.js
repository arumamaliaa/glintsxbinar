require("dotenv").config({
    path: `.env.${process.env.NODE_ENV}`,
});
// Express
const express = require("express");
const fileUpload = require("express-fileupload"); // Import express-fileupload

// Import router
const transaksiRoutes = require("./routes/transaksiRoutes");
const barangRoutes = require("./routes/barangRoutes");

// Make app
const app = express();

// Body parser
app.use(express.json()); // Enable json req.body
app.use(
    express.urlencoded({
        extended: false,
    })
); // Enable req.body urlencoded

// To read form-data
app.use(fileUpload());

// Static folder (for images)
app.use(express.static("public"));

// Make routes
app.use("/transaksi", transaksiRoutes);
app.use("/barang", barangRoutes);

// Run server
app.listen(3000, () => console.log("Server running on 3000"));