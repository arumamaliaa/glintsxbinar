const validator = require("validator");
const mongoose = require("mongoose");
const { barang, pelanggan, pemasok, transaksi } = require("../../models");

exports.getOne = (req, res, next) => {

    if (!mongoose.Types.ObjectId.isValid(req.params.id)) {
        return res.status(400).json({
            message: "Parameter is not valid and must be 24 characters & hexadecimal",
        });
    }

    next();
};

exports.create = async (req, res, next) => {
    let errors = [];

    try {
        console.log(req.body.id_pemasok)
        if (!mongoose.Types.ObjectId.isValid(req.body.id_pemasok)) {
            errors.push(
                "id_pemasok is not valid and must be 24 characters & hexadecimal",
            );
        }

        if (errors.length > 0) {
            return res.status(400).json({
                message: errors.join(", "),
            });
        }

        let dataPemasok = await pemasok.findOne({ _id: req.body.id_pemasok });

        if (!dataPemasok) {
            errors.push("Pemasok not found");
        }

        if (!validator.isNumeric(req.body.harga)) {
            errors.push("Harga must be a number");
        }

        if (errors.length > 0) {
            return res.status(400).json({
                message: errors.join(", "),
            });
        }

        req.body.pemasok = req.body.id_pemasok;

        next();

    } catch (error) {
        return res.status(500).json({
            error,
        })

    }
};

exports.update = async (req, res, next) => {
    let errors = [];

    if (!mongoose.Types.ObjectId.isValid(req.params.id)) {
        errors.push(
            "id_barang is not valid and must be 24 characters & hexadecimal",
        );
    }

    if (!mongoose.Types.ObjectId.isValid(req.params.id_pemasok)) {
        errors.push(
            "id_pemasok is not valid and must be 24 characters & hexadecimal",
        );
    }

    if (errors.length > 0) {
        return res.status(400).json({
            message: errors.join(", "),
        });
    }

    let dataPemasok = await pemasok.findOne({ _id: req.body.id_pemasok });

    if (!dataPemasok) {
        errors.push("Pemasok not found");
    }

    if (!validator.isNumeric(req.body.harga)) {
        errors.push("Harga must be a number");
    }

    if (errors.length > 0) {
        return res.status(400).json({
            message: errors.join(", "),
        });
    }

    req.body.pemasok = dataPemasok;

    next();
};

exports.delete = async (req, res, next) => {
    let errors = [];

    if (!mongoose.Types.ObjectId.isValid(req.params.id)) {
        errors.push(
            "id_barang is not valid and must be 24 character & hexadecimal"
        );
    }

    if (errors.length > 0) {
        return res.status(400).json({
            message: errors.join(", "),
        });
    }

    let data = await barang.findOne({ _id: req.params.id });

    // If transaksi not found
    if (!data) {
        errors.push("Barang not found");
    }

    if (errors.length > 0) {
        return res.status(400).json({
            message: errors.join(", "),
        });
    }

    next();
};