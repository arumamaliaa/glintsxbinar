const crypto = require("crypto");
const path = require("path");
const { barang, pelanggan, pemasok, transaksi } = require("../models");

class BarangController {
    // Get all data
    async getAll(req, res) {
        try {
            // Get all transaksi data
            let data = await barang.find();

            // If no data
            if (data.length === 0) {
                return res.status(404).json({
                    message: "Barang Not Found",
                });
            }

            // If success
            return res.status(200).json({
                message: "Success",
                data,
            });
        } catch (e) {
            return res.status(500).json({
                message: "Internal Server Error",
                error: e,
            });
        }
    }

    async getOne(req, res) {
        try {
            // Get all transaksi data
            let data = await barang.findOne({ _id: req.params.id });

            // If no data
            if (!data) {
                return res.status(404).json({
                    message: "Barang Not Found",
                });
            }

            // If success
            return res.status(200).json({
                message: "Success",
                data,
            });
        } catch (e) {
            return res.status(500).json({
                message: "Internal Server Error",
                error: e,
            });
        }
    }



    async create(req, res) {
        try {
            if (req.files) {
                const file = req.files.image;

                if (!file.mimetype.startsWith("image")) {
                    return res.status(400).json({ message: "File must be an image "});
                }

                if (file.size > 1000000) {
                    return res
                    .status(400)
                    .json({ message: "Image must be less than 1MB"});
                }

                let fileName = crypto.randomBytes(16).toString("hex");

                file.name = `${fileName}${path.parse(file.name).ext}`;

                req.body.image = file.name;

                file.mv(`./public/images/${file.name}`, async (err) => {
                    if (err) {
                        console.log(err);

                        return res.status(500).json({
                            message: "Internal Server Error",
                            error: err,
                        });
                    }
                });
            }

            let createdData = await barang.create(req.body);

            let data = await barang
            .findOne({ _id: createdData._id })
            .populate("pemasok");

            return res.status(201).json({
                message: "Success",
                data,
            });
        } catch (e) {
            console.error(e);
            return res.status(500).json({
                message: "Internal Server Error",
                error: e,
            });
        }
    }

    async update(req, res) {
                try {
                    // Update data
                    let data = await barang.findOneAndUpdate(
                        {
                            _id: req.params.id,
                        },
                        req.body, // This is all of req.body
                        {
                            new: true,
                        }
                    );
                    // new is to return the updated transaksi data
                    // If no new, it will return the old data before updated

                    // If success
                    return res.status(201).json({
                        message: "Success",
                        data,
                    });
                } catch (e) {
                    return res.status(500).json({
                        message: "Internal Server Error",
                        error: e,
                    });
                }
            }

            async delete (req, res) {
                try {
                    // delete data
                    await barang.delete({ _id: req.params.id });

                    return res.status(200).json({
                        message: "Success",
                    });
                } catch (e) {
                    return res.status(500).json({
                        message: "Internal Server Error",
                        error: e,
                    });
                }
            }
        }
   
module.exports = new BarangController();