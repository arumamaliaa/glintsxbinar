const express = require("express");
const validator = require("validator");
// Import validator

// Import controller
const barangController = require("../controllers/barangController");
const barangValidator = require("../middlewares/validators/barangValidator");

// Make router
const router = express.Router();

// Get all transaksi
router.get("/", barangController.getAll);
router.get("/:id", barangValidator.getOne, barangController.getOne);
router.post("/", barangValidator.create, barangController.create);
router.put("/:id", barangValidator.update, barangController.update);
router.delete("/:id", barangValidator.delete, barangController.delete);

module.exports = router;