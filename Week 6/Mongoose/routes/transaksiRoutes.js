const express = require("express");
const validator = require("validator");

// Import validator

// Import controller
const transaksiController = require("../controllers/transaksiController");
const transaksiValidator = require("../middlewares/validators/transaksiValidator");
// Make router
const router = express.Router();

// Get all transaksi
router.get("/", transaksiController.getAll);
router.get("/:id", transaksiValidator.getOne, transaksiController.getOne);
router.post("/", transaksiValidator.create, transaksiController.create);
router.put("/:id", transaksiValidator.update, transaksiController.update);
router.delete("/:id", transaksiValidator.delete, transaksiController.delete);

module.exports = router;