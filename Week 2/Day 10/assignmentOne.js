// declare array
let food = ["Tomato", "Brocolli", "Kale", "Cabbage", "Apple"];

// use for
for (let i = 0; i < food.length - 1; i++) {
    console.log(food[i] + ` is a healthy food, it's definitely worth to eat!`);
}

for (let i = 4; i < food.length; i++) {
    console.log(food[i] + ` Apple isn't vegetable!`);
}