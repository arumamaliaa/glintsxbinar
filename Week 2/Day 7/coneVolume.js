// Import readline
const readline = require("readline");
const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
});

function isEmptyOrSpaces(str) {
  return str === null || str.match(/^ *$/) !== null;
}

/* Cone Volume */
function calculateConeVolume(r, h) {
    let phi = 3.14;
    
    return 1/3 * phi * r ** 2 * h;
}

function input() {
    rl.question("Radius: ", (r) => {
      rl.question("Height: ", (h) => {
          if (!isNaN(r) && !isNaN(h)) {
            console.log(
              `Cone volume is ${calculateConeVolume(
                r,
                h
              )} cm3 \n`
            );
  
            rl.close();
          } else {
            console.log("The input must be number! \n");
            input();
          }
        });
    });
  }
  
input();

module.exports.rl = rl; 
module.exports.isEmptyOrSpaces = isEmptyOrSpaces;
