const data = require('./arrayFactory.js');
const test = require('./test.js');

function clean(data) {
  return data.filter(i => typeof i === 'number');
}

function sortAscending(data) {
  data = clean(data)
  let go = true
 
  while(go){
    for(let i = 0; i < data.length; i++){
      if(data[i] > data[i + 1]){
        let temp = data[i] 
        data[i] = data[i+1]
        data[i+1] = temp
        break;
       
      } else if(data[i] == data[i + 1] || data[i] < data[i + 1]) {
        continue
      } else {
        go =false
        
      }   
    } 
  }
  console.log(data)
  return data;
}

function sortDecending(data) {
  data = clean(data)
  let go = true
  
  while(go){
    for(let i = 0; i < data.length; i++){
      if(data[i] < data[i + 1]){
        let temp = data[i]
        data[i] = data[i+1]
        data[i+1] = temp
        break;
      } else if(data[i] == data[i + 1] || data[i] > data[i + 1]) {
        
      } else {
        go =false
      }
    }
  }
  console.log(data)
  return data;
}
sortAscending(data)
sortDecending(data)

test(sortAscending, sortDecending, data);