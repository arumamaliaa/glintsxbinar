const express = require ("express");
const app = express();
const helloRoute = require("./routes/helloRoute");

app.get("/", helloRoute);

app.listen(3000, () => console.log("Server running on 3000!"));