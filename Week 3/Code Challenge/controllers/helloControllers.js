class HelloController {
    get(req, res) {
        console.log("This is my first backend");
        res.send(`Hello, Arum Amalia!`);
    }

    post(req, res) {
        console.log("This is my POST");
        res.send("Hello, Postman (POST)!");
    }

    put(req, res) {
        console.log("This is my PUT");
        res.send("Hello, Postman (PUT)!");
    }

    delete(req, res) {
        console.log("This is my DELETE");
        res.send("Hello, Postman (DELETE)!");
    }
}

module.exports = new HelloController();

// ${req.query.name}!