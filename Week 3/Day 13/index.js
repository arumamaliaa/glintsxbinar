const Beam = require("./beam");
const Cube = require("./cube");
const Tube = require("./tube");
const Cone = require("./cone");

module.exports = { Beam, Cube, Tube, Cone };
