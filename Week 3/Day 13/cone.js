const ThreeDimention = require("./threeDimention");

class Cone extends ThreeDimention {
  constructor(pi, radius, height) {
    super("Cone");

    this.pi = pi;
    this.radius = radius;
    this.height = height;
  }

  // Overloading
  introduce(who) {
    super.introduce();
    console.log(`${who}, This is ${this.name}!`);
  }

  // Overridding
  calculateVolume() {
    let volume = 1 / 3 * this.pi * this.radius * this.height;

    console.log(`${this.name} volume is ${volume} cm3 \n`);
  }

}

let coneOne = new Cone(3.14, 9, 8);
coneOne.introduce("Lia");
coneOne.calculateVolume();

module.exports = Cone;