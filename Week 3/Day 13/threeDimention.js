const Geometry = require("./geometry");

class ThreeDimention extends Geometry {
  constructor(name) {
    super(name, "3D");

    if (this.constructor == ThreeDimention) {
      throw new Error("Can not declare object!");
    }
  }

  // Overridding
  introduce() {
    super.introduce();
    console.log(`This is ${this.type}!`);
  }

}

module.exports = ThreeDimention;
