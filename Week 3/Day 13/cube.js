const ThreeDimention = require("./threeDimention");

class Cube extends ThreeDimention {
  constructor(area) {
    super("Cube");

    this.area = area;
  }

  // Overloading
  introduce(who) {
    super.introduce();
    console.log(`${who}, This is ${this.name}!`);
  }

  // Overridding
  calculateVolume() {
    let volume = this.area ** 3;

    console.log(`${this.name} volume is ${volume} cm3 \n`);
  }

}

let cubeOne = new Cube(11);
cubeOne.introduce("Lia");
cubeOne.calculateVolume();

module.exports = Cube;
