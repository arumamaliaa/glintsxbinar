const ThreeDimention = require("./threeDimention");

class Tube extends ThreeDimention {
  constructor(pi, radius, height) {
    super("Tube");

    this.pi = pi;
    this.radius = radius;
    this.height = height;
  }

  // Overloading
  introduce(who) {
    super.introduce();
    console.log(`${who}, This is ${this.name}!`);
  }

  // Overridding
  calculateVolume() {
    let volume = this.pi * this.radius ** 2 * this.height;

    console.log(`${this.name} volume is ${volume} cm3 \n`);
  }

}

let tubeOne = new Tube(3.14, 6,7);
tubeOne.introduce("Lia");
tubeOne.calculateVolume();

module.exports = Tube;