const ThreeDimention = require("./threeDimention");

class Beam extends ThreeDimention {
  constructor(length, width, height) {
    super("Beam");

    this.length = length;
    this.width = width;
    this.height = height;
  }

  // Overloading
  introduce(who) {
    super.introduce();
    console.log(`${who}, This is ${this.name}!`);
  }

  // Overridding
  calculateVolume() {
    let volume = this.length * this.width * this.height;

    console.log(`${this.name} volume is ${volume} cm3 \n`);
  }

}

let beamOne = new Beam(5, 3, 4);
beamOne.introduce("Lia");
beamOne.calculateVolume();

module.exports = Beam;